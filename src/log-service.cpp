//
// Created by thephosphorus on 11/6/19.
//
// user-service.cpp : Defines the entry point for the application.
//
#include "LogHandler.h"

#include <yxlib/ConfigHelper.h>
#include <yxlib/RestServer.h>
#include <yxlib/yxstream.h>
#include <yxlib/oauthHandler.h>

using namespace yx;

constexpr auto logConfigFile = "/etc/yx/log/config.yaml";

void helloWorld(const Rest::Request &req, Rest::Response res,
                LogHandler *logHandler) {
  if (logHandler) {
    logHandler->sync();
    res.sendJson(restbed::OK, req.body());
    return;
  }
  res.send(restbed::INTERNAL_SERVER_ERROR, "HelloWorld");
}

void logRequestHandler(const Rest::Request &req, Rest::Response res,
                       LogHandler *logHandler) {

  const std::string userToken = req.accessToken();

  if (!OauthHandler::isvalidToken(userToken)) {
    yx::cout() << userToken
               << " doesn't have access to logs"
               << std::endl;
    res.send(restbed::FORBIDDEN, "You need Admin access");
    return;
  }

  logHandler->sync();
  constexpr auto lastField = "dernier";

  size_t last = 0;

  Rest::Json body = req.body();

  if (body.contains(lastField)) {
    last = body.at(lastField);
  }

  std::vector<Log> arr;

  arr = (last == 0) ? logHandler->getLast20() : logHandler->getLogsSince(last);

  constexpr auto informationsField = "information";

  Rest::Json information;

  for (const Log &log : arr) {
    Rest::Json node;
    node["no"] = log.timeStamp;
    node["severite"] = log.type;
    node["heure"] = log.time;
    node["message"] = log.source + " - " + log.message;

    information.emplace_back(node);
  }

  Rest::Json newBody;
  newBody[informationsField] = information;

  res.sendJson(restbed::OK, newBody);
}

int main() {

  // Add Config files
  ConfigHelper::addFile(logConfigFile);

  LogHandler logs;

  // Create Server
  RestServer server;
  // Add Endpoints (this will be refactored after for a better endpoint
  // handeling
  server.get("/admin/logs/hello", std::bind(helloWorld, std::placeholders::_1,
                                            std::placeholders::_2, &logs));
  server.get("/admin/logs/serveurweb",
             std::bind(logRequestHandler, std::placeholders::_1,
                       std::placeholders::_2, &logs));

  yx::cout() << "Starting server listening on " << server.getHost()
             << " on port " << std::to_string(server.getPort()) << std::endl;
  server.start();
  return EXIT_SUCCESS;
}
