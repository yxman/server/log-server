//
// Created by thephosphorus on 11/6/19.
//

#ifndef LOG_SERVICE_LOGHANDLER_H
#define LOG_SERVICE_LOGHANDLER_H

#include <bits/types/time_t.h>

#include <fstream>
#include <map>
#include <string>
#include <vector>

struct Log {
  time_t timeStamp;
  std::string type;
  std::string time;
  std::string source;
  std::string message;
};

class LogHandler {
public:
  LogHandler(); // Default Constructor (uses ConfigHelper)

  void addLogFile(const std::string &fileName);

  static Log ParseLine(const std::string &logLine);

  [[nodiscard]] std::vector<Log> getLogsSince(time_t time) const;

  [[nodiscard]] std::vector<Log> getLast20() const;

  void sync();

private:
  std::vector<std::ifstream> _files;
  std::multimap<time_t, Log> _logs;
};

#endif // LOG_SERVICE_LOGHANDLER_H
