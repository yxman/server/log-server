//
// Created by thephosphorus on 11/6/19.
//

#include "LogHandler.h"
#include <yxlib/ConfigHelper.h>
#include <yxlib/yxstream.h>

#include <algorithm>

using namespace yx;

LogHandler::LogHandler() {
  // Load Config Section
  ConfigSection section = ConfigHelper::getSection("logs");

  const auto logFiles = section.getStrArr("logFiles");

  for (const std::string &fileName : logFiles) {
    addLogFile(fileName);
  }
}
void LogHandler::addLogFile(const std::string &fileName) {
  std::ifstream file(fileName, std::ios::in);

  if (!file.is_open()) {
    yx::cerr() << "Could not open log file " << fileName << std::endl;
  } else {
    _files.emplace_back(std::move(file));
  }
}
Log LogHandler::ParseLine(const std::string &logLine) {
  Log log;

  std::stringstream stream(logLine);

  uint64_t timetmp;

  // Get Timestamp
  stream >> timetmp;
  log.timeStamp = timetmp;
  stream.ignore(3, '[');
  stream >> log.type >> log.time >> log.source;
  stream.ignore(3, ']');
  std::getline(stream, log.message);

  return log;
}
std::vector<Log> LogHandler::getLogsSince(time_t time) const {
  std::vector<Log> logs;
  auto it = std::find_if(_logs.rbegin(), _logs.rend(),
                         [time](const std::pair<time_t, Log> &elem) {
                           return elem.first < time;
                         })
                .base();

  for (; it != _logs.end(); it++) {
    if (it->second.timeStamp != time) {
      logs.emplace_back(it->second);
    }
  }

  return logs;
}
void LogHandler::sync() {
  for (std::ifstream &file : _files) {
    while (file.peek() != EOF) {
      std::string line;
      std::getline(file, line);
      if (!line.empty()) {
        Log log = LogHandler::ParseLine(line);
        _logs.insert(std::make_pair(log.timeStamp, log));
        std::cout << "Added " << line << std::endl;
      }
    }
    file.clear();
  }
}
std::vector<Log> LogHandler::getLast20() const {
  std::vector<Log> logs;
  logs.reserve(20);

  auto rIt = _logs.rbegin();

  for (size_t i = 0; i < 20 && rIt != _logs.rend(); ++i, ++rIt)
    ;

  auto it = rIt.base();

  for (; it != _logs.end(); it++) {
    logs.emplace_back(it->second);
  }

  return logs;
}
